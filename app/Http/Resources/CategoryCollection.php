<?php

namespace App\Http\Resources;

use App\Models\Post;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JsonSerializable;

class CategoryCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
