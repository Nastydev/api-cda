<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostUrlResource extends JsonResource
{
    public function toArray($request)
    {
        return route('categories.index', ['category' => $this->category]);
    }
}
