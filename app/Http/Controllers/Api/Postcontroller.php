<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use App\Models\Post;
use Illuminate\Http\Request;

class Postcontroller extends Controller
{

    public function index()
    {
//        $posts = Post::query()->get();
//
//        return response()->json(['success' => true, 'users' => new PostCollection($posts)]);
        return new PostCollection(Post::all());

    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
