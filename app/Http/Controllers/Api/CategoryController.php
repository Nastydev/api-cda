<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        return new CategoryCollection(Category::all());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return Category::create($request->all());
    }


    /**
     * @param $id
     * @return CategoryResource
     */
    public function show($id)
//    public function show($id)
    {
        return new CategoryResource(Category::findOrFail($id));

    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
       return Category::find($id)->update($request->all());
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        return Category::destroy($id);
    }

}
