<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\MovieController;
use App\Http\Controllers\Api\Postcontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
//Route::post('/categories/{}', [\App\Http\Controllers\CategoryController::class, 'index']);
//Route::put('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
//Route::delete('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);

Route::apiResource('categories', CategoryController::class);
Route::apiResource('movies', MovieController::class);
Route::apiResource('posts', Postcontroller::class);

//Route::get('/posts', [Postcontroller::class, 'index'])->name('posts.index');

